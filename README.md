DATA from DENAMIC project
========================================

Data from DENAMIC project are available from Downloads folder. The folder contain the following files:

* DENAMIC_Endosulfan.zip
	- Pre-processed RNA-seq data
	- Pre-processed miRNA-seq data
	- Pre-processed proteomic data
	- Pre-processed metabolomic data
	- Results from cognitive and motor coordination tests